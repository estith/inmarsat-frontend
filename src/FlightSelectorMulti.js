import React, {useState} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import CampaignSelector from './CampaignSelector';
import FlightSelector from './FlightSelectorControl';
import AircraftSelector from './AircraftSelectorControl';


export default function FlightSelectorMulti(props) {
    const [aircraft, setAircraft] = useState(null);
    const [campaigns, setCampaigns] = useState(null);
  
    return (
    <>
        <Typography gutterBottom>
          <CampaignSelector onChange={setCampaigns} />
        </Typography>
        <Typography gutterBottom>
          <AircraftSelector onChange={setAircraft} />
        </Typography>
        <Typography gutterBottom>
          <FlightSelector onChange={props.onChange} campaigns={campaigns} aircraft={aircraft} />
        </Typography>
    </>)

}
