import React, { ReactDOM, useState, useEffect, setItems } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import TailSelector from './TailSelector';
import StatusRow from './StatusRow';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function StatusTable(props) {

  const [tailNumber, setTailNumber] = useState('INTEST');

  const classes = useStyles();
  const table_id = props.id + '_tail_number';

  /*function changeTail(event) {
    console.log(event.target.value);
    setTailNumber(event.target.value);
  }*/

  return (
    <div id={props.id}>
    <Typography variant="h6">
      <TailSelector id={table_id} onChange={(event) => {console.log(event.target.value); setTailNumber(event.target.value); return false;}}  />
    </Typography>
    <Divider />
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Tx</TableCell>
            <TableCell align="right">Rx</TableCell>
            <TableCell align="right">Responding</TableCell>
            <TableCell align="right">SDU State</TableCell>
            <TableCell align="right">Link</TableCell>
            <TableCell align="right">Signal 1</TableCell>
            <TableCell align="right">Signal 2</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <StatusRow tail_number={tailNumber} />
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  );
}
