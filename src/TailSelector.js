import React, { useState, useEffect, setItems } from 'react';

import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

export default function TailSelector(props) {

    const [aircraftList, setAircraftList] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch('http://localhost:5000/list_aircraft')
          .then(res => res.json())
          .then(
            (result) => {
              setIsLoaded(true);
              setAircraftList(result);
            },
            (error) => {
              setIsLoaded(true);
              setError(error);
            }
          )
      }, []);

    if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        let tail_number = '<empty>';
        if(typeof aircraftList[0] !== 'undefined') {
            tail_number = aircraftList[0].tail_number;      
        }
        return (
            <TextField id={props.id} label="Aircraft" value={tail_number} onChange={props.onChange} select>
            {aircraftList.map(aircraft => (
                <MenuItem value={aircraft.tail_number}>{aircraft.display_name}</MenuItem>
            ))}
            </TextField>
        );
      }
    }
