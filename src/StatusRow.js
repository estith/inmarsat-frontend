import React, { useState, useEffect, setItems } from 'react';

import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';


function createData(tx, rx, responding, sdu_state, link, signal0, signal1) {
    return { tx, rx, responding, sdu_state, link, signal0, signal1 };
}
  
  export default function StatusRow(props) {
  
    const [aircraftStatus, setAircraftStatus] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [error, setError] = useState(null);
  
    function loadData() {
      fetch('http://localhost:5000/list_flights?tail_number=' + props.tail_number)
      .then(res => res.json())
      .then(
        (result) => {
            if(result.length > 0) {
                let status = result[0];
                let statusData = createData(status.tx, status.rx, status.api_responding, status.sdu_link_state, status.unit_status, status.signal_strength_0, status.signal_strength_1);
                setAircraftStatus(statusData);      
                setIsLoaded(true);
            }
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
    
    }
  
    useEffect(loadData, [aircraftStatus]);
  
    if (error) {
      return <TableRow>Error: {error.message}</TableRow>;
    } else if (!isLoaded) {
      return <TableRow>Loading...</TableRow>;
    } else {
      return <TableRow>
          <TableCell component="th" scope="row">{aircraftStatus.tx}</TableCell>
          <TableCell align="right">{aircraftStatus.rx}</TableCell>
          <TableCell align="right">{aircraftStatus.responding}</TableCell>
          <TableCell align="right">{aircraftStatus.sdu_state}</TableCell>
          <TableCell align="right">{aircraftStatus.unit_status}</TableCell>
          <TableCell align="right">{aircraftStatus.signal0}</TableCell>
          <TableCell align="right">{aircraftStatus.signal1}</TableCell>
        </TableRow>
    }
  }
  
  