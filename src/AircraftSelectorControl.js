import React, { useState, useEffect, setItems } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 96;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 480,
    },
  },
};

export default function AircraftSelector(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [isLoaded, setIsLoaded] = useState(false);
  const [tailNumber, setTailNumber] = useState(null);
  const [aircraft, setAircraft] = useState(null);
  const [aircraftList, setAircraftList] = useState([]);
  const [aircraftSelectedList, setSelectedList] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch('http://localhost:5000/list_aircraft')
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setAircraftList(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, []);

  // Render the selected string.
  const renderValue = (selectList) => {
    let labels = [];

    selectList.map((aircraft) => labels.push(aircraft.display_name))
    return labels.join(', ');
  };

  // Run when the selection is changed.
  const change = (event) => {
    setSelectedList(event.target.value);
    if('onChange' in props) {
      props.onChange(event.target.value);
    }
  };
  
  // If data isn't loaded, return an error.
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  }

  return (
    <FormControl className={classes.formControl}>
        <InputLabel id="aircraft-checkbox-label">Aircrafts</InputLabel>
        <Select
          labelId="aircraft-checkbox-label"
          id="aircraft-checkbox"
          multiple
          value={aircraftSelectedList}
          onChange={change}
          input={<Input />}
          renderValue={renderValue}
          MenuProps={MenuProps}
        >
          {aircraftList.map((aircraft) => (
            <MenuItem key={aircraft.display_name} value={aircraft}>
              <Checkbox checked={aircraftSelectedList.indexOf(aircraft) > -1} />
              <ListItemText primary={aircraft.display_name} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
  );
}
