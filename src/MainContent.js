import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import StatusTable from './StatusTable';
import VisualizationTabs from './VisualizationTabs';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

export default function MainContent() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                <Paper square>
                    <h2>SDU Status</h2>
                    <StatusTable id="main_status_table" />
                </Paper>
                </Grid>
                <Grid item xs={12}>
                <Paper square>
                    <h2>Data Use Visualizations</h2>
                    <VisualizationTabs />
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}


