import React, { useState, useEffect, setItems, useMemo } from 'react';

import { Chart } from 'react-charts'
//https://www.npmjs.com/package/react-charts#quick-example
 
export default function DataHistogram(props) {
  const [isLoaded, setIsLoaded] = useState(false);
  const [error, setError] = useState(null);

  const [data, setData] = useState(useMemo(
    () => [
      {
        label: 'Bytes',
        data: [['Paper', 4.56], [1, 2], [2, 4.456], [3, 2], [4, 7]]
      },
    ],
    []
  ));
 
  const axes = useMemo(
    () => [
      { primary: true, type: 'ordinal', position: 'bottom' },
      { type: 'linear', position: 'left' }
    ],
    []
  )

  const series = useMemo(
    () => ({
      type: 'bar'
    }),
    []
  )

  useEffect(() => {
    console.log(['::::::::', props.flights])
    fetch('http://localhost:5000/get_stats?flights=' + props.flights)
      .then(res => res.json())
      .then(
        (result) => {
          let labels = {};
          console.log(['+++', result]);

          setIsLoaded(true);
          result.map((stat) => {
            if(stat.instrument.label in labels && Number.isInteger(stat.byte_count)) {
              labels[stat.instrument.label] += stat.byte_count;
            }
            else {
              labels[stat.instrument.label] = parseInt(stat.byte_count);
            }
          });
          let data = [];
          for(let [label, value] of Object.entries(labels)) {
            data.push({x: label, y: props.type === 'aggregate' ? value: value / 10000 /* replace with a time calculation */});
          }
   
          let struct = [
              {
                label: props.type === 'aggregate' ? 'Bytes': 'Bytes/Hour',
                data: data,
              },
          ];

          setData(struct);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, []);

  // If data isn't loaded, return an error.
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  }
  console.log(data);
  const chart = (
    // A react-chart hyper-responsively and continuusly fills the available
    // space of its parent element automatically
    <div
      style={{
        width: '100%',
        height: '300px'
      }}
    >
      <Chart data={data} axes={axes} series={series} tooltip />
    </div>
  )

  return chart;
}