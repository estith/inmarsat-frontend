import React, { useState } from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

import DataHistogram from './Histogram';
import FlightSelectorDialog from './FlightSelectorDialog';
import FlightSelectorMulti from './FlightSelectorMulti';
  
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500,
  },
  minorButton: {
    '& > *': {
      margin: theme.spacing(1),
    }
  }
}));


export default function VisualizationTabs() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = useState(0);
  const [flights, setFlights] = useState([]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  const change = (event) => {
    console.log(['#####', event]);
    let flights = [];
    event.map((v) => {
      flights.push(v.id);
    })
    console.log(['*****', flights]);
    setFlights(flights);
  }

  const handleChangeIndex = (index) => {
    setValue(index);
  };
//<FlightSelectorDialog />
        
  return (
    <div className={classes.root}>
      <div class={classes.minorButton}>
        <FlightSelectorMulti onChange={change} />

      </div>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Aggregate (KB)" {...a11yProps(0)} />
          <Tab label="Rate (KB/Hour)" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          Aggregate (KB)
          <DataHistogram type="aggregate" flights={flights} />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          Rate (KB/Hour)
          <DataHistogram type="rate" flights={flights} />
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}
