import React, { useState, useEffect, setItems } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';

import CampaignSelector from './FlightSelectorControl';
import AircraftSelector from './AircraftSelectorControl';

const matchId = (o, l) => {
  console.log(l)
  for(let i = 0; i < l.length; i++) {
    if(l[i].id === o.id) {
      return true;
    }
  }
  return false;
};

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 96;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 480,
    },
  },
};

export default function FlightSelector(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [isLoaded, setIsLoaded] = useState(false);
  const [flightList, setFlightList] = useState([]);
  const [flightSelectedList, setSelectedList] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch('http://localhost:5000/list_flights')
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setFlightList(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, []);

  // Render the selected string.
  const renderValue = (selectList) => {
    let labels = [];

    selectList.map((flight) => labels.push(flight.start_time))
    return labels.join(', ');
  };

  // Run when the selection is changed.
  const change = (event) => {
    setSelectedList(event.target.value);
    if('onChange' in props) {
      props.onChange(event.target.value);
    }
  };

  // If data isn't loaded, return an error.
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  }

  let displayList = [];
  flightList.map((flight) => {
    console.log(['props: ', props])
    if(  (!('aircraft' in props) || props.aircraft === null || props.aircraft.length == 0 || matchId(flight.aircraft, props.aircraft))
      && (!('campaigns' in props) || props.campaigns === null || props.campaigns.length == 0 || matchId(flight.campaign, props.campaigns))) {
        displayList.push(flight);
    }
  });

  return (
    <FormControl className={classes.formControl}>
        <InputLabel id="flight-checkbox-label">Flights</InputLabel>
        <Select
          labelId="flight-checkbox-label"
          id="flight-checkbox"
          multiple
          value={flightSelectedList}
          onChange={change}
          input={<Input />}
          renderValue={renderValue}
          MenuProps={MenuProps}
        >
          {displayList.map((flight) => (
            <MenuItem key={flight.start_time} value={flight}>
              <Checkbox checked={flightSelectedList.indexOf(flight) > -1} />
              <ListItemText primary={flight.start_time} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
  );
}
